import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-medicament',
  templateUrl: './medicament.component.html',
  styleUrls: ['./medicament.component.scss']
})
export class MedicamentComponent implements OnInit {
  formValue !:FormGroup;

  constructor(private formbuider:FormBuilder, private api:ApiService) { }

  ngOnInit(): void {
    this.formValue=this.formbuider.group({
      ID:[''],
      Code_cip:[''],
      Nom_medicament:[''],
      Forme:[''],
      Dosage:[''],
      Notice_medicament:[''],
      Conditionnement_medicament:[''],
      Marque_Medicament:['']
    })
  }
  

}
