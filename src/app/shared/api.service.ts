import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'
import{map} from 'rxjs/operators'
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }
  postMedicament(data:any){
    return this.http.post<any>("",data).pipe(map((res:any)=>{
      return res;
    }))
  }
  getMedicament(){
    return this.http.get<any>("").pipe(map((res:any)=>{
      return res;
    }))

  }
  updateMedicament(data:any,id:number){
    return this.http.post<any>("",data).pipe(map((res:any)=>{
      return res;
    }))
  }
}
