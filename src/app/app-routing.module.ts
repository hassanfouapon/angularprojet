import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageAccueilComponent } from './page-accueil/page-accueil.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { MedicamentComponent } from './medicament/medicament.component';
const routes: Routes = [
  {path:'',component:PageAccueilComponent},
  {path:'login',component:LoginComponent},
  { path: 'medicament',   component:MedicamentComponent },
  {path:'inscription',component:InscriptionComponent},
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
