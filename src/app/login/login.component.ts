import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
user= new User();
message='';
  constructor(private _service:RegistrationService, private _router: Router) { }

  ngOnInit(): void {
  }
loginUser(){
  this._service.loginUserFromRemote(this.user).subscribe(
    data =>{
      console.log("response recieved");
      this._router.navigate([''])

    } ,
    error => {
      console.log("exception occured")
      this.message="erreur svp entrer le bon nom d'utilisateur et le le vrai mot de passe"
  }
  )
  
}
}
